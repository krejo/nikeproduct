//
//  NikeProductTests.swift
//  NikeProducts
//
//  Created by JOSEPH KERR on 7/16/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import XCTest
@testable import NikeProducts

class NikeProductTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    
    func testInitNikeProduct() {
        
        let product = NikeProduct(name: "name1", price: "555", color: "abc", imageThumnailURLString: "thumburl", imagePrimaryURLString: "primaryurl")
        
        XCTAssertNotNil(product)
    }

    
    func testFailableInit1() {
        var attrs: [String: AnyObject] = [:]
        attrs["name1"] = "testName"
        
        let product = NikeProduct(attributes: attrs)
        XCTAssertNil(product)
    }
    
    func testFailableInit2() {
        var attrs: [String: AnyObject] = [:]
        attrs["name1"] = "testName"
        attrs["colorCode"] = "abc"
        
        let product = NikeProduct(attributes: attrs)
        XCTAssertNil(product)
    }
    
    func testFailableInit3() {
        var attrs: [String: AnyObject] = [:]
        attrs["name1"] = "testName"
        attrs["colorCode"] = "abc"
        
        let prices : [String: AnyObject] = ["price1" : "1.99"]
        attrs["prices"] = prices
        
        let product = NikeProduct(attributes: attrs)
        XCTAssertNil(product)
    }

    func testFailableInitSucceeds() {
        var attrs: [String: AnyObject] = [:]
        attrs["name1"] = "testName"
        attrs["colorCode"] = "abc"
        
        let prices : [String: AnyObject] = ["price1" : "1.99",
                                            "currentRetail":"2.99"]
        attrs["prices"] = prices
        
        let product = NikeProduct(attributes: attrs)
        
        XCTAssertNotNil(product)
        
        XCTAssertEqual(product!.price, "2.99")
        XCTAssertEqual(product!.name, "testName")
        XCTAssertEqual(product!.color, "abc")
    }


    
    


}
