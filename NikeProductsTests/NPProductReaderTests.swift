//
//  NPProductsTests.swift
//  NikeProducts
//
//  Created by JOSEPH KERR on 7/16/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import XCTest
@testable import NikeProducts

class NPProductReaderTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }

    
    func testReaderThrowsFileError() {
        
        var productReader = NikeProductReader()
        productReader.fname = "foo"
        
        XCTAssertThrowsError(try productReader.readProducts({
            (products) in
            
        }),"Throws") { (error) in
            
            XCTAssertEqual(error as? ReadDataError, .FileError)
        }
        
        
    }
    
    func testReaderThrowsParserError() {
        var productReader = NikeProductReader()
        productReader.fname = "productList_corrupt"
        
        XCTAssertThrowsError(try productReader.readProducts({
            (products) in
            
            XCTFail("Error not thrown")
            
        }),"Throws") { (error) in
            
            XCTAssertEqual(error as? ReadDataError, .ParseError)
        }
    }

    
    func testReaderThrowsProductsDataNotFound() {
        var productReader = NikeProductReader()
        productReader.fname = "productList_err_noproductskey"
        
        XCTAssertThrowsError(try productReader.readProducts({
            (products) in
            XCTFail("Error not thrown")

        }),"Throws") { (error) in
            
            XCTAssertEqual(error as? ReadDataError, .ProductsDataNotFound)
        }
    }

    
    func testReadFileWithOneProduct() {
        let expectation = expectationWithDescription("SomeService")
        let productReader = NikeProductReader()
        
        do {
            try productReader.readProducts() {
                (products) in
                
                let results = products.flatMap{$0}
                XCTAssertEqual(results.count, 1)
                expectation.fulfill()
            }
        }
        catch  {
            XCTFail("Error thrown \(error) unexpected")
        }
        
        waitForExpectationsWithTimeout(0.25) { (error) in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }

    }
    
    func testReadFileWithMultipleProducts() {
        
        let expectation = expectationWithDescription("SomeService")
        
        var productReader = NikeProductReader()
        productReader.fname = "productList_multiple"

        do {
            try productReader.readProducts() {
                (products) in
                
                let results = products.flatMap{$0}
                XCTAssertEqual(results.count, 2)
                expectation.fulfill()
            }
        }
        catch  {
            XCTFail("Error thrown \(error) unexpected")
        }
        
        waitForExpectationsWithTimeout(0.25) { (error) in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
        
    }
    
    func testReadFileWithManyProducts() {
        
        let expectation = expectationWithDescription("SomeService")
        
        var productReader = NikeProductReader()
        productReader.fname = "productList_many"
        
        do {
            try productReader.readProducts() {
                (products) in
                
                let results = products.flatMap{$0}
                XCTAssertEqual(results.count, 4)
                expectation.fulfill()
            }
        }
        catch  {
            XCTFail("Error thrown \(error) unexpected")
        }
        
        waitForExpectationsWithTimeout(0.25) { (error) in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }

    

}
