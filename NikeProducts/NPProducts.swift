//
//  NPProducts.swift
//  NikeProducts
//
//  Created by JOSEPH KERR on 7/12/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import Foundation

// MARK: Product Data Record

/**
 Product data record
 
 Use method `readProducts` to read the file and obtain [NikeProduct?])
 
     readProducts(productsAvailable: (products: [NikeProduct?])
 
 */

struct NikeProduct {
    var name: String?
    var price: String?
    var color: String?
    
    var imageThumnailURLString: String?
    var imagePrimaryURLString: String?
}


extension NikeProduct {
    
    init?(attributes:[String: AnyObject]){
        
        guard
        let name = attributes["name1"] as? String,
        let color = attributes["colorCode"] as? String,
        let price = attributes["prices"]?["currentRetail"] as? String
            else {
                return nil
        }

        self.name = name
        self.color = color
        self.price = price

        let imagePrimaryURLString  =
            (attributes["images"] as? [[NSObject : AnyObject]])?.filter{
            $0["type"] as? String == "primary"
        }.first?["path"] as? String
            
        self.imagePrimaryURLString = imagePrimaryURLString
        
        let imageThumnailURLString  =
            (attributes["images"] as? [[NSObject : AnyObject]])?.filter{
                $0["type"] as? String == "thumbnail"
                }.first?["path"] as? String

        self.imageThumnailURLString = imageThumnailURLString
    }
}


// MARK: Product Record Generator

enum ReadDataError: ErrorType {
    case FileError
    case FileDataError
    case ParseError
    case ProductsDataNotFound
    case ResultsDataNotFound
}


/**
 Read JSON data from file in project bundle
 
 Parse the JSON and return items struct NikeProduct { format
 
 - construct the file URL to bundle
 - read the file
 - parse the JSON
 - return the results
 
 Use method `readProducts` to read the file
 
        readProducts(productsAvailable: (products: [NikeProduct?])
 
 */

struct NikeProductReader {

    var fname: String?

    init() { }

    init(with filename: String) {
        self.fname = filename
    }
    
    func readProducts(productsAvailable: (products: [NikeProduct?]) -> Void ) throws -> Void {
        
        try readProductsFromFile(productsAvailable)
    }
    
    func readProductsFromFile(productsAvailable: (products: [NikeProduct?]) -> Void ) throws -> Void  {
        
        let productsJSONFname = fname ?? "productList"
        
        guard let productURL = NSBundle.mainBundle().URLForResource(productsJSONFname, withExtension: "json")
            else {
                throw ReadDataError.FileError
        }
        
        guard let productData = NSData(contentsOfURL: productURL)
            else {
                throw ReadDataError.FileDataError
        }
        
        try parseDataForProducts(productData,productsAvailable: productsAvailable)
    }

    
    func parseDataForProducts(productData:NSData, productsAvailable: (products: [NikeProduct?]) -> Void ) throws  -> Void {
        
        guard let jsonData = try? NSJSONSerialization.JSONObjectWithData(productData, options:[]) as! [NSObject : AnyObject]
            else {
                throw ReadDataError.ParseError
        }

        try parseResultsForProducts(jsonData, productsAvailable: productsAvailable)
    }
    
    
    /// Drills into JSON to obtain the desired data
    ///
    /// Usage:
    ///
    ///     try parseResultsForProducts(jsonData, productsAvailable: { } )
    ///
    /// - parameters:
    ///   - Dictionary : jsonData, JSONObjectWithData
    ///   - Closure : productsAvailable
    /// - throws:
    /// - returns: Void
    
    func parseResultsForProducts(jsonData:[NSObject : AnyObject], productsAvailable: (products: [NikeProduct?]) -> Void ) throws  -> Void {
        
        guard let results = jsonData["results"] as? [[NSObject : AnyObject]]
            else {
                throw ReadDataError.ResultsDataNotFound
        }
        
        guard let products = results.first?["products"] as? [[String : AnyObject]]
            else {
                throw ReadDataError.ProductsDataNotFound
        }

        
        productsAvailable(products: products.map( {
            
            (product) -> NikeProduct? in
            
            NikeProduct(attributes: product)
        })
        )
    }
    
}






