//
//  NPProductImageViewController.swift
//  NikeProducts
//
//  Created by JOSEPH KERR on 7/12/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

class NPProductImageViewController: UIViewController {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBAction func didTap(sender: AnyObject) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }

}
