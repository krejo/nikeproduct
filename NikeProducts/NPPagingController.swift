//
//  NPPagingController.swift
//  NikeProducts
//
//  Created by JOSEPH KERR on 7/15/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

/**
 Protocol to allow our datasource to communicate with a delegate
 
 */

protocol NPPagingDataSourceDelegate: class {
    func paging(controller:NPPagingDatasource?, itemAtIndex index:Int) -> NikeProduct?
    func pagingNumberOfPages(controller:NPPagingDatasource?) -> Int
}


/**
 Our implementation of UIPageViewControllerDataSource
 
 */

class NPPagingDatasource: NSObject, UIPageViewControllerDataSource {
    
    weak var delegate: NPPagingDataSourceDelegate?
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        var result: UIViewController?
        if let sourcePage = (viewController as! NPProductViewController).pageIndex where (sourcePage > 0) {
            result = viewControllerAtIndex(sourcePage - 1)
        }
        return result;
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {

        var result: UIViewController?
        if let numberOfPages = delegate?.pagingNumberOfPages(self),
            sourcePage = (viewController as! NPProductViewController).pageIndex where (sourcePage < numberOfPages - 1) {
            result = viewControllerAtIndex(sourcePage + 1)
        }
        return result
    }
    
    func viewControllerAtIndex(index: Int) -> UIViewController? {
        
        var result: UIViewController?
        let productViewController = UIStoryboard(name: "Main",
            bundle:nil).instantiateViewControllerWithIdentifier("NPProductView") as! NPProductViewController
        
        if let product = delegate?.paging(self, itemAtIndex: index) {
            productViewController.pageIndex = index
            productViewController.product = product
        }
        result = productViewController
        return result
    }
    
}
