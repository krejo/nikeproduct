//
//  NPProductViewController.swift
//  NikeProducts
//
//  Created by JOSEPH KERR on 7/12/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

class NPProductViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    
    var product: NikeProduct?
    var pageIndex: Int?

    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let _ = product {
            self.nameLabel.text = self.product?.name
            self.priceLabel.text = self.product?.price
            self.colorLabel.text = self.product?.color
            
            if let imageURLString = self.product!.imageThumnailURLString {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    if let imageURL = NSURL(string: imageURLString),
                        imageData = NSData(contentsOfURL:imageURL),
                        productImage = UIImage(data: imageData) {
                        
                        dispatch_async(dispatch_get_main_queue()){
                            self.productImageView.image = productImage
                        }
                    }
                }
            }
            
        } else {
            nameLabel.text = ""
            priceLabel.text = ""
            colorLabel.text = ""
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "NPPresentProductImageSegue" {
            
            if let productImageViewController = segue.destinationViewController as? NPProductImageViewController,
                imageURLString = product!.imagePrimaryURLString {
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    if let imageURL = NSURL(string: imageURLString),
                        imageData = NSData(contentsOfURL:imageURL),
                        productImage = UIImage(data: imageData) {
                        
                        dispatch_async(dispatch_get_main_queue()){
                            productImageViewController.productImageView.image = productImage
                            productImageViewController.activity.stopAnimating()
                        }
                    }
                }
            }
            
        }  // NPPresentProductImageSegue
        
    }  // func
    

}


