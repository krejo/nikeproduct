//
//  AppDelegate.swift
//  NikeProducts
//
//  Created by JOSEPH KERR on 7/12/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, NPPagingDataSourceDelegate {

    var window: UIWindow?

    /*
     Typically, one does not use the AppDelegate as a place to store Data or be a delegate to some 
     functional operation.  A new data object would be created to handle that.
     Here, the appdelegate is used for simplicity
     */
    var products: [NikeProduct]?
    var pagingDatasource: NPPagingDatasource?
    

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

        // Override point for customization after application launch.
        
        // productList_many
        // productList_multiple
        // productList_corrupt
        // productList_err_noproductskey
        // productList

        let productReader = NikeProductReader(with: "productList_many")
        do {
            try productReader.readProducts() {
                (products) in
                self.products = products.flatMap{$0}
            }
        }
        catch  {
            print("Error \(error)")
        }

        
        // Obtaining the pageViewController would still be done had the dataModel been 
        // implemented in another object
        //    pagingDatasource!.delegate = self
        //    -- would be replaced by
        //    pagingDatasource!.delegate = thatObject


        if let pagingViewController = self.window?.rootViewController as? UIPageViewController {

            pagingDatasource = NPPagingDatasource()
            
            pagingDatasource!.delegate = self
            
            pagingViewController.dataSource = pagingDatasource!
            
            if let startingViewController = pagingDatasource!.viewControllerAtIndex(0) {
                pagingViewController.setViewControllers([startingViewController], direction: .Forward, animated: false, completion: nil)
            }
        }
        
        return true
    }
    
    func paging(controller:NPPagingDatasource?, itemAtIndex index:Int) -> NikeProduct? {

        var result: NikeProduct?
        if let product = products?[index] {
            result = product
        }
        return result
    }
    
    func pagingNumberOfPages(controller:NPPagingDatasource?) -> Int {

        var result: Int = 0
        if let productsCount = products?.count {
            result = productsCount
        }
        return result
    }
    
    
    
    // MARK: Application Events
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

