# README #

A Swift2 project is an exercise that reads and parses a JSON file.

The Manager is requesting you to take the following coding exercise.  Ideally this should be back by close of business today.
 
Please complete this in a shorter timeframe (approx.., 2hrs.):
 
iOS Coding Question:
·         Create a new iOS App using either Swift or Objective-C
·         Time box your effort to two hour
 
Requirements:
 
1.     Use the attached JSON response and parse the response using the mechanism of your choice.
2.     For each Product in the JSON data, display the below contents:
·         Product name1
·         A product thumbnail image
·         Price
·         Color
3.     When a product is tapped, present a larger product image
4.     Still have time? Bonus:
·         Make the App work in both Landscape and Portrait Modes
 
** Unit test cases are mandatory
 

### Purpose ###

* This project is an exercise handed to me from Nike to read and parse a JSON file of Nike products.
* version 1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Setup ###

* Open Xcode project in Xcode7
* Command-U to run Tests
* Supports iPhone and iPad

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact